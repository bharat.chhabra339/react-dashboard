import { makeStyles } from "@material-ui/core";

export const HeaderStyle = makeStyles((theme) => ({
  background: {
    backgroundColor: '#ffffff',
    paddingBottom: 50,
    paddingTop: 20,
  },
  MenuText: {
    marginLeft: 20,
    fontWeight: '600',
    borderColor: '#515576',
  },
  ButtonStyle: {
    marginTop: 40,
    width: '80%',
    alignSelf: 'center',
    backgroundColor: '#4772FF',
    color: '#ffffff',
    padding: 10,
    fontWeight: 'bold',
  },
  TextStyle: {
    marginTop: 40,
  },
}));
