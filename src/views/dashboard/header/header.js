import {Box, CardMedia, Grid, makeStyles, Typography} from '@material-ui/core';
import React, {useState} from 'react';
import {bell } from '../../../utils/images';
import { HeaderStyle } from './styles';


function Header(props) {
  const classes = HeaderStyle();
  const [state, setstate] = useState('welcome');
  return (
    <Grid lg={12} xl={12} md={12} sm={12} alignItems={'center'} justify={'center'} container>
      <Grid alignItems={'center'} container item md={4}>
        <Box
          onClick={() => setstate('welcome')}
          borderBottom={state === 'welcome' ? 3 : 0}
          className={classes.MenuText}>
          <Typography variant={'body1'}>Welcome</Typography>
        </Box>
        <Box
          onClick={() => setstate('overview')}
          borderBottom={state === 'overview' ? 3 : 0}
          className={classes.MenuText}>
          <Typography variant={'body1'}>Overview</Typography>
        </Box>
        <Box
          onClick={() => setstate('cards')}
          borderBottom={state === 'cards' ? 3 : 0}
          className={classes.MenuText}>
          <Typography variant={'body1'}>Cards</Typography>
        </Box>
      </Grid>
      <Grid container alignItems={'center'} justify="flex-end" item md={4}>
        <Box
          alignItems={'center'}
          justifyContent="center"
          border={1}
          borderRadius={20}
          padding={1.2}
          borderColor={'blue'}>
          <Typography variant={'body1'}>Current Balance : $2313213</Typography>
        </Box>
      </Grid>
      <Grid
        alignItems={'center'}
        container
        justify={'space-between'}
        item
        md={4}>
        <Typography
          style={{marginLeft: 70}}
          variant={'body1'}
          className={classes.MenuText}>
          Payment History
        </Typography>
        <CardMedia image={bell} style={{height: 30, width: 30}} />
      </Grid>
    </Grid>
  );
}

export default Header;
