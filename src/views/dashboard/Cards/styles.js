const { makeStyles } = require("@material-ui/core");

export const useStyles = makeStyles((theme) => ({
  color: {
    color: '#ffffff',
  },
  numberStyle: {
    marginTop: 20,
  },
}));
