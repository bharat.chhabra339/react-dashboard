import {Box, Card, Grid, makeStyles, Typography} from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';
import {useStyles} from './styles'
const DebitCards = () => {
  const classes = useStyles();
  return (
    <Grid md>
        <Cardhead borderRadius={20}
        borderColor={'rgba(0,0,0.1)'}
        boxShadow={1}
        padding={3}>
          <Typography variant={'h5'} className={classes.color}>
            VISA
          </Typography>
          <Typography
            className={[classes.color, classes.numberStyle]}
            variant={'h6'}>
            **** **** **** 4765
          </Typography>
          <Grid container alignItems={'center'} justify={'space-between'}>
            <Box>
              <Typography className={classes.color} variant={'body2'}>
                CARD HOLDER
              </Typography>
              <Typography className={classes.color} variant={'body2'}>
                Jakub
              </Typography>
            </Box>
            <Box margin={1} alignItems={'center'} justify={'space-between'}>
              <Typography className={classes.color} variant={'body2'}>
                VALID THRU
              </Typography>
              <Typography className={classes.color} variant={'body2'}>
                11 / 22
              </Typography>
            </Box>
          </Grid>
        </Cardhead>
    </Grid>
  );
};

const Cardhead = styled(Box)`
  background: #bb6fff; /* fallback for old browsers */
  background: -webkit-linear-gradient(
    to right,
    #bb6fff,
    #e08eff
  ); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(
    to right,
    #bb6fff,
    #e08eff
  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  border-radius: 20px;
`;

export default DebitCards;
