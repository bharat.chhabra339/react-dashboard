import { Box, Grid, Typography } from '@material-ui/core';
import styled from 'styled-components';
export const Image = styled.img`
  border-radius: 10px;
  height: 40px;
  width: 40px;
`;
export const RowView = styled(Grid)`
  align-items: center;
`;
export const Subtitle = styled(Typography)`
  margin-left: 20px;
  margin-top: 10px;
`;
export const CardRow = styled(Grid)`
  justify-content: space-between;
  margin-right: 20px;
  margin-left: 20px;
  align-items: center;
  margin-top: 30px;
`;
export const ScrollView = styled(Box)`
  border-radius: 20px;
`;
export const Header = styled(Typography)`
  font-weight: bold !important;
  color: #545776 !important;
  font-size: 14px !important;
`;
export const PriceText = styled(Typography)`
  font-weight: bold !important;
  color: #66d2ca !important;
`;
export const ToggleView = styled.div`
  margin-top: 30px;
  margin-bottom: 20px;
`;
