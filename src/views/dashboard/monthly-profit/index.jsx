import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  Subtitle,
  CardRow,
  PriceText,
  Header,
  ToggleView,
} from './styles';
import {Doughnut} from 'react-chartjs-2';
import {FormControlLabel, Grid, Switch, withStyles} from '@material-ui/core';
const MonthlyProfit = (props) => {
  const [state, setstate] = useState(true);
  let lineData = {
    labels: ['January', 'February', 'March', 'April', 'May'],
    datasets: [
      {
        label: 'Rainfall',
        backgroundColor: [' #e08eff', '#e08eff', '#e08eff', '#e08eff'],
        hoverBackgroundColor: [' #e08eff', '#e08eff', '#e08eff', '#e08eff'],
        data: [65, 59, 80, 81, 10],
      },
      {
        label: 'Rainfall',
        backgroundColor: [
          '#e08eff',
          '#e08eff',
          '#e08eff',
          '#e08eff',
          '#e08eff',
        ],
        hoverBackgroundColor: [' #e08eff', '#e08eff', '#e08eff', '#e08eff'],
        data: [20, 59, 50, 81, 100],
      },
      {
        label: 'Rainfall',
        backgroundColor: ['#e08eff', '#e08eff', '#e08eff', '#e08eff'],
        hoverBackgroundColor: [' #e08eff', '#e08eff', '#e08eff', '#e08eff'],
        data: [65, 59, 80, 81, 56],
      },
    ],
  };
  const purple='#e08eff';
 const IOSSwitch = withStyles((theme) => ({
   root: {
     width: 42,
     height: 26,
     padding: 0,
     margin: theme.spacing(1),
   },
   switchBase: {
     padding: 1,
     '&$checked': {
       transform: 'translateX(16px)',
       color: theme.palette.common.white,
       '& + $track': {
         backgroundColor: purple,
         opacity: 1,
         border: 'none',
       },
     },
     '&$focusVisible $thumb': {
       color: purple,
       border: '6px solid #fff',
     },
   },
   thumb: {
     width: 24,
     height: 24,
   },
   track: {
     borderRadius: 26 / 2,
     border: `1px solid ${theme.palette.grey[400]}`,
     backgroundColor: theme.palette.grey[50],
     opacity: 1,
     transition: theme.transitions.create(['background-color', 'border']),
   },
   checked: {},
   focusVisible: {},
 }))(({classes, ...props}) => {
   return (
     <Switch
       focusVisibleClassName={classes.focusVisible}
       disableRipple
       classes={{
         root: classes.root,
         switchBase: classes.switchBase,
         thumb: classes.thumb,
         track: classes.track,
         checked: classes.checked,
       }}
       {...props}
     />
   );
 });

    const handleChange = (event) => {
      setstate(!state);
    };
  return (
    <Grid>
      <ScrollView
        boxShadow={2}
        border={1}
        padding={3}
        borderColor={'rgba(0,0,0,.1)'}>
        <div>
          <Header>Monthly Profit</Header>
          <ToggleView>
            <FormControlLabel
              control={
                <IOSSwitch
                  checked={state}
                  onChange={handleChange}
                  name="checkedB"
                />
              }
            />
          </ToggleView>
        </div>
        <Doughnut
          data={lineData}
          type="bar"
          options={{
            responsive: true,
            maintainAspectRatio: true,
            legend: {
              display: false,
              position: 'right',
            },
            title: {
              position: 'bottom',
              text: 'Chart',
              display: false,
            },
          }}
        />
        <CardRow>
          <Header>Sarrah Williams</Header>
          <PriceText>$3300</PriceText>
        </CardRow>
      </ScrollView>
    </Grid>
  );
};

export default MonthlyProfit;
