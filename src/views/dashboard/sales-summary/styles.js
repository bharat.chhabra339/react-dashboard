import styled from "styled-components";

export const ExchangImage = styled.img`
  height: 10px;
  width: 10px;
  margin-left: 10px;
  margin-right: 10px;
`;