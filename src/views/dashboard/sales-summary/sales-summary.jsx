import React from 'react';
import {Line} from 'react-chartjs-2';
import {Box, CardActions, Grid, Typography} from '@material-ui/core';
import { exchange } from '../../../utils/images';
//Line chart
let lineData = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
  datasets: [
    {
      label: 'Income',
      borderWidth: 2,
      backgroundColor: 'rgba(224, 142, 255, 0.5)',
      borderColor: '#E08EFF',
      pointBorderColor: '#E08EFF',
      pointBackgroundColor: '#E08EFF',
      data: [0, 10, 6, 7, 25, 9, 3, 12],
    },
  ],
};

const SalesSummary = () => {
  return (
    <Grid>
      <Box borderRadius={20} borderColor={'rgba(0,0,0.1)'} boxShadow={1} padding={1}>
        <div
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            display: 'flex',
          }}>
          <Typography variant={'body1'}>Exchange Rates</Typography>
          <div
            style={{
              flexDirection: 'row',
              display: 'flex',
              alignItems: 'center',
            }}>
            <Typography variant={'body1'}>USD</Typography>
            <img
              src={exchange}
              style={{height: 10, width: 10, marginRight: 10, marginLeft: 10}}
            />
            <Typography variant={'body1'}>Eur</Typography>
          </div>
        </div>
        <Line
          data={lineData}
          type="bar"
          options={{
            maintainAspectRatio: true,
            legend: {
              display: false,
              labels: {fontFamily: 'Nunito Sans'},
            },
            scales: {
              yAxes: [
                {
                  stacked: true,
                  gridLines: {display: false},
                  ticks: {fontFamily: 'Nunito Sans'},
                },
              ],
              xAxes: [
                {
                  gridLines: {display: false},
                  ticks: {fontFamily: 'Nunito Sans'},
                },
              ],
            },
          }}
        />
      </Box>
    </Grid>
  );
};

export default SalesSummary;
