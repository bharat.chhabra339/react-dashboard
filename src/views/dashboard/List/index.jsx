

import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {FixedSizeList} from 'react-window';
import {Box, Grid, Typography} from '@material-ui/core';
import { useStyles } from './list';

function renderRow(props) {
  const {index, style} = props;
  const classes = useStyles();

  return (
    <ListItem className={classes.list} button style={style} key={index}>
      <div className={classes.rowStyle}>
        <img
          className={classes.avatar}
          src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50"
        />
        <Typography className={classes.nameStyle}>John</Typography>
      </div>
      <Typography>{'$250'}</Typography>
    </ListItem>
  );
}

renderRow.propTypes = {
  index: PropTypes.number.isRequired,
  style: PropTypes.object.isRequired,
};

export default function LastCost() {
  const classes = useStyles();

  return (
    <Grid md>
      <Box
        borderRadius={20}
        borderColor={'rgba(0,0,0.1)'}
        boxShadow={1}
        className={classes.root}>
        <Typography style={{marginLeft: 20, paddingTop: 10}}>
          Last Costs
        </Typography>
        <FixedSizeList height={150} width={'auto'} itemSize={55} itemCount={20}>
          {renderRow}
        </FixedSizeList>
      </Box>
    </Grid>
  );
}
