import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: 190,
    maxWidth: 340,
    backgroundColor: theme.palette.background.paper,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 10,
  },
  list: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rowStyle: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
  },
  nameStyle: {
    marginLeft: 20,
    color: 'grey',
  },
}));
