import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Sidebar from '../../common/Sidebar';
import { useStyles } from './styles';



export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Sidebar />
    </div>
  );
}
