import React from 'react';
import {
  makeStyles,
  Table,
  TableHead,
  TableRow,
  withStyles,
  TableCell,
  Box,
  Typography,
} from '@material-ui/core';
import {check, circle, down_arrow, ellipse} from '../../../utils/images';
import {StyledTableCell, useStyles} from './styles';

const IncomeTable = (props) => {
  const classes = useStyles();
  const data = [
    {name: 'jakub', price: '$212', color: classes.greenColor},
    {name: 'Bharat', price: '$221'},
    {name: 'Bharat', price: '$333', color: classes.greenColor},
    {name: 'Bharat', price: '$411'},
    {name: 'Bharat', price: '$534', color: classes.greenColor},
    {name: 'Bharat', price: '$643'},
    {name: 'Bharat', price: '$755'},
    {name: 'jakub', price: '$856', color: classes.greenColor},
  ];
  return (
    <React.Fragment>
      <div className="container">
        <div className={classes.HeaderStyle}>
          <Typography variant={'body1'}>Income</Typography>
          <strong>...</strong>
        </div>
        <Table>
          <TableHead>
            <TableRow>
              <StyledTableCell>Select</StyledTableCell>
              <StyledTableCell align="center">Name</StyledTableCell>
              <StyledTableCell align="center">Amount</StyledTableCell>
              <StyledTableCell align="center">Date</StyledTableCell>
              <StyledTableCell align="center">Status</StyledTableCell>
            </TableRow>
          </TableHead>

          <tbody>
            <TableRow style={{backgroundColor: '#E08EFF'}}>
              <StyledTableCell>
                <Box
                  borderRadius={10}
                  borderColor={'#ffffff'}
                  border={1}
                  width={40}
                  padding={0.6}
                  className={classes.bg}>
                  <img src={check} className={classes.checkImage} />
                </Box>
              </StyledTableCell>
              <StyledTableCell className={classes.color} align="center">
                {'John'}
              </StyledTableCell>
              <StyledTableCell className={classes.color} align="center">
                {'$560'}
              </StyledTableCell>
              <StyledTableCell className={classes.color} align="center">
                19-08-2020
              </StyledTableCell>
              <StyledTableCell align="center">
                <img className={classes.image} src={down_arrow} />
              </StyledTableCell>
            </TableRow>

            {data.map((a) => {
              return (
                <TableRow>
                  <StyledTableCell>
                    <img
                      src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50"
                      className={classes.avatar}
                    />
                  </StyledTableCell>
                  <StyledTableCell align="center">{a.name}</StyledTableCell>
                  <StyledTableCell className={a.color} align="center">
                    {a.price}
                  </StyledTableCell>
                  <StyledTableCell align="center">19-08-2020</StyledTableCell>
                  <StyledTableCell align="center">
                    <img className={classes.image} src={circle} />
                  </StyledTableCell>
                </TableRow>
              );
            })}
          </tbody>
        </Table>
      </div>
    </React.Fragment>
  );
};

export default IncomeTable;
