const { TableCell, makeStyles, withStyles } = require("@material-ui/core");

export const useStyles = makeStyles((theme) => ({
  color: {
    color: '#ffffff',
  },
  numberStyle: {
    marginTop: 20,
  },
  greenColor: {
    color: '#61D1C8',
  },
  image: {
    height: 20,
    width: 20,
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  bg: {
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  checkImage: {
    height: 20,
    width: 20,
  },
  HeaderStyle:{
    justifyContent: 'space-between',
    alignItems: 'center',
    display: 'flex',
    flexDirection:'row',
    padding:10, 
  }
}));
export const StyledTableCell = withStyles((theme) => ({
  head: {
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);
