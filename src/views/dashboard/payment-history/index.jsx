import { Box, CardActions, Grid, Typography } from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import React from 'react';
import { history, reply } from '../../../utils/images';
import {
  StarView,
  ReviewContainer,
  IncomeHeader,
  PriceText,
  DetailText,
  HistoryText,
  CheckAllText,
  Image,
  ReplyIcon,
  CardStyle,
} from './styles';
const CostList = (props) => {
  const data = [
    { name: 'jakub', price: '320.00 ' },
    { name: 'Bharat', price: '300.00' },
    { name: 'jakub', price: '2000.00' },
  ];
  return (
    <Grid >
      <Box padding={3}>
        <Box>
          {data.map((a) => {
            return (
              <Grid container direction="row" alignItems="center">
                <Image src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" />
                <Grid style={{marginTop: 20, marginLeft: 30}}>
                  <IncomeHeader>Paypal Income</IncomeHeader>
                  <PriceText>+{a.price} USD</PriceText>
                  <DetailText>View Details</DetailText>
                </Grid>
              </Grid>
            );
          })}
          <Grid
            container
            direction="row"
            alignItems="center"
            style={{
              marginTop: 10,
            }}>
            <Image src={history} />
            <Grid style={{marginLeft: 30}}>
              <HistoryText>View all history</HistoryText>
              <CheckAllText>check all history</CheckAllText>
            </Grid>
          </Grid>
        </Box>
      </Box>
      <Box padding={3}>
        <Box>
          <h4>Recent Reviews</h4>
          <Grid
            container
            direction="row"
            alignItems="center"
            justify={'space-between'}>
            <StarView>
              <Typography>5.0</Typography>
              <Rating
                style={{marginLeft: 10}}
                name="size-medium"
                defaultValue={5}
              />
            </StarView>
            <Typography style={{margin: 0}}>17 reviews</Typography>
          </Grid>
          <hr></hr>
          {data.map((a) => {
            return (
              <>
                <CardActions>
                  <Grid container direction="row" alignItems="center">
                    <Image src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" />
                    <div style={{marginLeft: 20}}>
                      <Typography>Emma</Typography>
                      <Rating name="size-medium" defaultValue={5} />
                    </div>
                  </Grid>
                  <Grid direction="row" alignItems="center">
                    <ReplyIcon src={reply} />
                    <Typography>Reply</Typography>
                  </Grid>
                </CardActions>
                <Typography style={{marginTop: 10}}>
                  Contrary to popular belief, Lorem Ipsum is not simply random
                  text. It has roots in a piece of classical Latin literature
                  from 45 BC, making it over 2000 years old.
                </Typography>
              </>
            );
          })}
        </Box>
      </Box>
    </Grid>
  );
};


export default CostList;
