import styled from 'styled-components';
import { Box, Grid, Typography } from '@material-ui/core';

export const Rating = styled.div``;
export const StarView = styled(Grid)`
  align-items: center;
  flex-direction:row;
  display:flex;
`;
export const IncomeHeader = styled(Typography)`
  font-weight: bold !important;
  color:black !important
`;
export const PriceText = styled(Typography)`
  font-weight: bold !important;
  color: #66d2ca !important;
`;
export const DetailText = styled(Typography)`
  color: #cfccdc !important;
`;
export const HistoryText = styled(Typography)`
  font-weight: bold !important;
  color: #cfccdc !important;
`;
export const CheckAllText = styled(Typography)`
  color: #cfccdc !important;
`;
export const ReplyIcon = styled.img`
  height:20px;
  width:20px;
`;
export const Image = styled.img`
  border-radius: 20px;
  height: 60px;
  width: 60px;
`;
export const CardStyle = styled(Box)`
  border-radius:15px;
`;