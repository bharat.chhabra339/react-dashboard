import {makeStyles} from '@material-ui/core';

export const mainAuth = makeStyles((theme) => ({
  root: {
    flex: 1,
    backgroundColor: '#F8F8F8',
    [theme.breakpoints.down('sm')]: {
      backgroundColor: '#F2F6FF',
    },
    position: 'fixed',
    width: '100%',
    height: '100%',
  },
  MainContainer: {
    height: '100%',
    justifyContent: 'center',
  },
  leftSide: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
    backgroundColor: '#598DFF',
    height: '100%',
    borderBottomRightRadius: 100,
  },
  rightSide: {
    textAlign: 'center',
    backgroundColor: '#598DFF',
    [theme.breakpoints.down('sm')]: {
      backgroundColor: '#F2F6FF',
    },
    height: '100%',
  },
  rightSubSide: {
    backgroundColor: '#F2F6FF',
    height: '100%',
    borderTopLeftRadius: 100,
  },
  logoText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    padding: 30,
  },
  imageContainer: {
    justifyContent: 'center',
    display: 'flex',
    marginTop: 30,
    marginBottom: 30,
  },
  media: {
    width: '50%',
    alignItems: 'center',
    paddingTop: '65%', // 16:9,
  },
  copyrightText: {
    color: '#ffffff',
    paddingTop: 40,
  },
  authBox: {
    backgroundColor: '#F8F8F8',
    width: '55%',
    [theme.breakpoints.down('sm')]: {
      width: '80%',
    },
  },
  containerRoot: {
    marginLeft: 40,
    marginTop: 20,
  },
  SignInText: {
    borderColor: '#4772FF',
    paddingBottom: 20,
  },
  SignUpText: {
    marginLeft: 25,
    borderColor: '#4772FF',
    paddingBottom: 20,
  },
  socialContainer: {
    paddingTop: 40,
  },
  iconsStyle: {
    height: 20,
    width: 20,
    margin: 20,
  },
}));
