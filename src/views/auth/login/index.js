import React from 'react';
import {Box, Button, Grid, TextField, Typography} from '@material-ui/core';
import {useHistory} from 'react-router-dom';
import { LoginStyle } from './styles';


const Login = (props) => {
  const classes = LoginStyle();
  const history = useHistory();
  return (
    <Box borderRadius={30} boxShadow={0.5} className={classes.background}>
      <TextField
        className={classes.textinput}
        id="standard-name"
        label="USERNAME"
      />
      <TextField
        className={classes.textinput}
        id="standard-name"
        label="EMAIL"
      />
      <TextField
        className={classes.textinput}
        id="standard-name"
        label="PASSWORD"
        type="password"
      />
      <Button
        className={classes.ButtonStyle}
        variant="contained"
        onClick={() => history.push('dashboard')}>
        Login
      </Button>
      <Typography
        variant={'body2'}
        className={classes.TextStyle}
        color={'secondary'}>
        Forgot Password ?
      </Typography>
    </Box>
  );
};

export default Login;
