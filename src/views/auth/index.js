import React, {useState} from 'react';
import {Typography, CardMedia, Grid, Box, Hidden} from '@material-ui/core';
import {mainAuth} from './styles';
import Login from './login';
import Register from './register';
import { facebook, instagram, linkedin, pinterest, twitter,phone,email } from '../../utils/images';

function Auth() {
  const classes = mainAuth();
  const [action, setAction] = useState('login');
  return (
    <div className={classes.root}>
      <Grid container md className={classes.MainContainer}>
        <Hidden only={['sm','xs']}>
          <Grid item md>
            <div fixed className={classes.leftSide}>
              <Typography variant="h5" className={classes.logoText}>
                My Discounted Labs
              </Typography>
              <div className={classes.imageContainer} maxWidth="lg">
                <CardMedia
                  className={classes.media}
                  title="Contemplative Reptile"
                  image="https://pngimg.com/uploads/doctor/doctor_PNG15988.png"
                />
              </div>
              <Typography
                className={classes.copyrightText}
                variant={'body1'}
                align="center">
                Copyright 2018, My Discounted Labs. All right reserved.
              </Typography>
            </div>
          </Grid>
        </Hidden>
        <Grid item md>
          <Grid className={classes.rightSide}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              className={classes.rightSubSide}>
              <Box bomdhadow={4} borderRadius={30} className={classes.authBox}>
                <Grid
                  className={classes.containerRoot}
                  container
                  direction="row"
                  alignItems="center">
                  <Box
                    className={classes.SignInText}
                    borderBottom={action === 'login' && 3}>
                    <Typography
                      onClick={() => setAction('login')}
                      color={action === 'login' ? 'green' : 'textSecondary'}
                      variant={'h5'}>
                      Sign in
                    </Typography>
                  </Box>
                  <Box
                    borderColor="primary.main"
                    className={classes.SignUpText}
                    borderBottom={action === 'register' && 3}>
                    <Typography
                      variant={'h5'}
                      color={action === 'register' ? 'green' : 'textSecondary'}
                      onClick={() => setAction('register')}>
                      Sign up
                    </Typography>
                  </Box>
                </Grid>
                {action === 'login' ? <Login /> : <Register />}
              </Box>
              <Grid
                className={classes.socialContainer}
                container
                alignItems="center"
                justify="center">
                <CardMedia image={linkedin} className={classes.iconsStyle} />
                <CardMedia image={instagram} className={classes.iconsStyle} />
                <CardMedia image={facebook} className={classes.iconsStyle} />
                <CardMedia image={twitter} className={classes.iconsStyle} />
                <CardMedia image={pinterest} className={classes.iconsStyle} />
              </Grid>
              <Grid
                className={classes.socialContainer}
                container
                alignItems="center"
                justify="center">
                <CardMedia image={phone} className={classes.iconsStyle} />
                <Typography
                  variant={'caption'}
                  color={'initial'}
                  align="center">
                  +91 ***982***1
                </Typography>
                <CardMedia image={email} className={classes.iconsStyle} />
                <Typography
                  color={'initial'}
                  variant={'caption'}
                  align="center">
                  demo@gmail.com
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default Auth;