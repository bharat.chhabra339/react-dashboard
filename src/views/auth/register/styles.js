import { makeStyles } from "@material-ui/core";

export const LoginStyle = makeStyles((theme) => ({
  background: {
    backgroundColor: '#ffffff',
    paddingBottom: 50,
    paddingTop: 20,
  },
  textinput: {
    marginTop: 20,
    width: '80%',
    alignSelf: 'center',
  },
  ButtonStyle: {
    marginTop: 40,
    width: '80%',
    alignSelf: 'center',
    backgroundColor: '#4772FF',
    color: '#ffffff',
    padding: 10,
    fontWeight: 'bold',
  },
  TextStyle: {
    marginTop: 40,
  },
}));
