import React from 'react';
import {Box, Button, TextField, Typography} from '@material-ui/core';
import { LoginStyle } from './styles';
const Register = (props) => {
  const classes = LoginStyle();
  return (
    <Box borderRadius={30} boxShadow={0.5} className={classes.background}>
      <TextField
        className={classes.textinput}
        id="standard-basic"
        label="FULL NAME"
      />
      <TextField
        className={classes.textinput}
        id="standard-basic"
        label="EMAIL"
      />
      <TextField
        className={classes.textinput}
        id="standard-basic"
        label="PASSWORD"
        type="password"
      />
      <Button
        className={classes.ButtonStyle}
        variant="contained"
        href="#contained-buttons">
        SignUp
      </Button>
      <Typography
        variant={'body2'}
        className={classes.TextStyle}
        color={'secondary'}>
        I have an Account ?
      </Typography>
    </Box>
  );
};

export default Register;
