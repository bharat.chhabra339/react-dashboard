import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Auth from '../views/auth';
import Dashboard from '../views/dashboard';

function Routes() {
  return (
    <div>
      <Switch>
        <Route path="/dashboard">
          <Dashboard />
        </Route>
        <Route path="/">
          <Auth />
        </Route>
      </Switch>
    </div>
  );
}
export default Routes;