const {makeStyles} = require('@material-ui/core');

const drawerWidth = 240;
export const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  root: {
    display: 'flex',
    backgroundColor: '#FFFFFF',
    alignItems: 'flex-start',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    // marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  rightmenuButton: {
    // marginRight: theme.spacing(2),
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: {
    toolbar: theme.mixins.toolbar,
    justifyContent: 'center',
    display: 'flex',
  },
  drawerPaper: {
    width: drawerWidth,
    alignItems: 'center',
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  centerLogoView: {position: 'absolute', right: 250, left: 250},
  centerLogo: {height: 40, width: 40},
  rightBurgerMenu: {position: 'absolute', right: 0},
  SecondGridView: {marginTop: 40},
  mainView: {marginTop: 50},
  mainLogo: {height: 80, width: 80},
}));
