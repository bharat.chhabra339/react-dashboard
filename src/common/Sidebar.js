import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import {Box, CardMedia, Grid, Paper} from '@material-ui/core';
import Header from '../views/dashboard/header/header';
import DebitCards from '../views/dashboard/Cards';
import IncomeTable from '../views/dashboard/Table';
import MonthlyProfit from '../views/dashboard/monthly-profit';
import CostList from '../views/dashboard/payment-history';
import SalesSummary from '../views/dashboard/sales-summary/sales-summary';
import LastCost from '../views/dashboard/List/index';
import {useStyles} from './styles';

function Sidebar(props) {
  const {window} = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [rightMenu, setRightMenu] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const handleRightDrawerToggle = () => {
    setRightMenu(!rightMenu);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar}>
        <CardMedia
          image="https://png2.cleanpng.com/sh/d7d0ed0251ce3c02881f36b0b83e1fff/L0KzQYm3VcAyN6R3iZH0aYP2gLBuTgpmepDzfeY2cHXogn77j71xbZZ3RdRydITygsPsjwQua5DyiOd9ZYKwfrb7lB9zc146edcDZUW7QIqBVcdjPF84Uao6OUm6Q4K8UsUzOWM6TKYBMkSxgLBu/kisspng-zeronet-peer-to-peer-bittorrent-computer-network-5ae8e5809857b4.398199731525212544624.png"
          className={classes.mainLogo}
        />
      </div>
      <List>
        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );
  const rightmenu = (
    <div>
      <List>
        {['Welcome', 'Overview', 'Cards'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        <Box
          style={{margin: 20, marginBottom: 20}}
          alignItems={'center'}
          justifyContent="center"
          border={1}
          borderRadius={20}
          padding={1}
          borderColor={'blue'}>
          <Typography variant={'body1'}>Current Balance : $231</Typography>
        </Box>
      </List>
      <Divider />
      <List>
        {['Payment History'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      {/* <AppBar position="fixed" className={classes.appBar}> */}
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          className={classes.menuButton}>
          <MenuIcon />
        </IconButton>
      </Toolbar>

      <div className={classes.rightBurgerMenu}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="end"
          onClick={handleRightDrawerToggle}
          className={classes.rightmenuButton}>
          <MenuIcon />
        </IconButton>
      </div>
      <div className={classes.centerLogoView}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          className={classes.menuButton}>
          <CardMedia
            image="https://png2.cleanpng.com/sh/d7d0ed0251ce3c02881f36b0b83e1fff/L0KzQYm3VcAyN6R3iZH0aYP2gLBuTgpmepDzfeY2cHXogn77j71xbZZ3RdRydITygsPsjwQua5DyiOd9ZYKwfrb7lB9zc146edcDZUW7QIqBVcdjPF84Uao6OUm6Q4K8UsUzOWM6TKYBMkSxgLBu/kisspng-zeronet-peer-to-peer-bittorrent-computer-network-5ae8e5809857b4.398199731525212544624.png"
            className={classes.centerLogo}
          />
        </IconButton>
      </div>

      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}>
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={'right'}
            open={rightMenu}
            onClose={handleRightDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}>
            {rightmenu}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open>
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown lgUp implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            anchor={'right'}
            open>
            {rightmenu}
          </Drawer>
        </Hidden>
      </nav>

      <main className={classes.content}>
        <Hidden smDown mdDown>
          <Header />
        </Hidden>
        <div className={classes.toolbar} />
        <main className={classes.mainView}>
          <Grid container direction={'row'} md={12} spacing={4}>
            <Grid direction="column" md={9}>
              <Grid container item md={12} spacing={4}>
                <Grid item xl={4} md={8} lg={4} sm={8} xs={10}>
                  <DebitCards />
                </Grid>
                <Grid item md={8} lg={4} sm={8} xs={8}>
                  <SalesSummary />
                </Grid>
                <Grid item md={12} xl={4} lg={4} sm={12} xs={12}>
                  <LastCost />
                </Grid>
              </Grid>
              <Grid
                className={classes.SecondGridView}
                container
                md={12}
                spacing={3}>
                <Grid item md={8}>
                  <IncomeTable />
                </Grid>
                <Grid item md={8} lg={4} sm={8} xs={8}>
                  <MonthlyProfit />
                </Grid>
              </Grid>
            </Grid>
            <Grid container xl={3} xs={12} md={8} lg={3} sm={12}>
              <CostList />
            </Grid>
          </Grid>
        </main>
      </main>
    </div>
  );
}

Sidebar.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default Sidebar;
